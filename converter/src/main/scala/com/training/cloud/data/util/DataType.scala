package com.training.cloud.data.util

object DataType {
  val EMPLOYEE = "EMPLOYEE"

  def availableDataTypes() = {
    val avaibaleFieldList = this.getClass.getDeclaredFields
    val avaibaleFieldString = avaibaleFieldList.map { field => field.getName.toUpperCase() }.mkString(",")
  }
}
