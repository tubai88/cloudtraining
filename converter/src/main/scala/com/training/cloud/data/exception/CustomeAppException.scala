package com.training.cloud.data.exception

final case class CustomeAppException(private val message: String = "",
                                     private val cause: Throwable = None.orNull)
    extends Exception(message, cause)
