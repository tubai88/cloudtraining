import datetime
import os

from airflow import models
from airflow.contrib.operators import dataproc_operator
from airflow.contrib.operators import bigquery_operator
from airflow.operators import python_operator
from airflow.utils import trigger_rule

# Path to jar file.
JAR_FILE = 'gs://trainingdata-kd/jar/converter-1.0.0-uber.jar'
stagingArea = 'gs://data-kd/'
outputBasePath = 'gs://trainingdata-kd/data/processed'

yesterday = datetime.datetime.combine(
    datetime.datetime.today() - datetime.timedelta(1),
    datetime.datetime.min.time())

default_dag_args = {
    # Setting start date as yesterday starts the DAG immediately when it is
    # detected in the Cloud Storage bucket.
    'start_date': yesterday,
    # To email on failure or retry set 'email' arg to your email and enable
    # emailing here.
    'email_on_failure': False,
    'email_on_retry': False,
    # If a task fails, retry it once after waiting at least 1 minutes
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=1),
    'project_id': 'cloudtraining-kd'
}

def getDataProcClusterName(**kwargs):
    triggerFileName = kwargs['dag_run'].conf.get('name')
    triggerFileParams = triggerFileName.split('_')
    dataset = triggerFileParams[0]
    dataYear = triggerFileParams[1]
    dataMonth = triggerFileParams[2]
    dataProcClusterName = dataset + '-converter-dataproc'
    return dataProcClusterName.lower()
    # Rest of the code

def getInputFileName(**kwargs):
    triggerFileName = kwargs['dag_run'].conf.get('name')
    input_path = triggerFileName
    return input_path
    # Rest of the code

def getBqSource(**kwargs):
    triggerFileName = kwargs['dag_run'].conf.get('name')
    triggerFileParams = triggerFileName.split('_')
    dataset = triggerFileParams[0]
    dataYear = triggerFileParams[1]
    dataMonth = triggerFileParams[2]
    output_path_sub = 'data/processed/' + dataset +'/' + dataYear + '/' + dataMonth
    bigQuerySource = 'data/processed/' + dataset +'/' + dataYear + '/' + dataMonth + '/*.avro'
    return bigQuerySource
    # Rest of the code

def getBqTableName(**kwargs):
    triggerFileName = kwargs['dag_run'].conf.get('name')
    triggerFileParams = triggerFileName.split('_')
    dataset = triggerFileParams[0]
    dataYear = triggerFileParams[1]
    dataMonth = triggerFileParams[2]
    bigQueryTable = 'cloudtraining-kd.' + dataset + '.' + dataset  + '_' + dataYear + '_' + dataMonth
    return bigQueryTable
    # Rest of the code

# https://www.linkedin.com/pulse/building-data-pipeline-airflow-mehmet-vergili - check this page
with models.DAG(
        'composer_sample_dataproc',
        schedule_interval=None,
        default_args=default_dag_args) as dag:

    # Process parameters
    parse_cluster_name = python_operator.PythonOperator(
        task_id='parse_cluster_name',
        python_callable=getDataProcClusterName,
        provide_context=True)

    # Process parameters
    parse_input_file = python_operator.PythonOperator(
        task_id='parse_input_file',
        python_callable=getInputFileName,
        provide_context=True)

    # Process parameters
    parse_bq_tablename = python_operator.PythonOperator(
        task_id='parse_bq_tablename',
        python_callable=getBqTableName,
        provide_context=True)

    # Process parameters
    parse_bq_source = python_operator.PythonOperator(
        task_id='parse_bq_source',
        python_callable=getBqSource,
        provide_context=True)

    # Create a Cloud Dataproc cluster.
    create_dataproc_cluster = dataproc_operator.DataprocClusterCreateOperator(
        task_id='create_dataproc_cluster',
        # Give the cluster a unique name by appending the date scheduled.
        # See https://airflow.apache.org/code.html#default-variables
        cluster_name = "{{ task_instance.xcom_pull(task_ids='parse_cluster_name') }}",
        zone = 'europe-west2-b',
        num_workers = 2,
        master_disk_size = 10,
        worker_disk_size = 10,
        image_version = '1.0-debian9',
        master_machine_type = 'n1-standard-1',
        worker_machine_type = 'n1-standard-1',
        bucket = 'trainingdata-kd')

    # Run Dataproc job
    run_dataproc_spark = dataproc_operator.DataProcSparkOperator(
        task_id = 'run_dataproc_spark',
        main_jar = JAR_FILE,
        cluster_name = "{{ task_instance.xcom_pull(task_ids='parse_cluster_name') }}",
        arguments = [stagingArea, "{{ task_instance.xcom_pull(task_ids='parse_input_file') }}", outputBasePath])

    # Create Bigquery Table
    load_data_to_bq = bigquery_operator.BigQueryCreateExternalTableOperator(
        task_id = 'load_data_to_bq',
        bucket = 'trainingdata-kd',
        source_objects = [ "{{ task_instance.xcom_pull(task_ids='parse_bq_source') }}" ],
        source_format = 'AVRO',
        destination_project_dataset_table = "{{ task_instance.xcom_pull(task_ids='parse_bq_tablename') }}")

    # Delete Cloud Dataproc cluster.
    delete_dataproc_cluster = dataproc_operator.DataprocClusterDeleteOperator(
        task_id = 'delete_dataproc_cluster',
        cluster_name = "{{ task_instance.xcom_pull(task_ids='parse_cluster_name') }}",
        # Setting trigger_rule to ALL_DONE causes the cluster to be deleted
        # even if the Dataproc job fails.
        trigger_rule = trigger_rule.TriggerRule.ALL_DONE)

    # Define DAG dependencies.
    parse_cluster_name >> parse_input_file >> parse_bq_tablename >> parse_bq_source >> create_dataproc_cluster >> run_dataproc_spark >> load_data_to_bq >> delete_dataproc_cluster