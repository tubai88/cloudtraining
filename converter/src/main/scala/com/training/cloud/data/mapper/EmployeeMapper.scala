package com.training.cloud.data.mapper

import com.training.cloud.data.model.Employee
import org.apache.avro.mapred.AvroKey
import org.apache.hadoop.io.NullWritable
import org.slf4j.LoggerFactory

object EmployeeMapper {
  val logger = LoggerFactory.getLogger(EmployeeMapper.getClass)
  
  def mapEmployee(dataLine: String) = {
    try{
      val dataFields = dataLine.split(",",-1)
      val id = dataFields(0)
      val firstName = dataFields(1)
      val middleName = dataFields(2)
      val surName = dataFields(3)
      val joiningDate = dataFields(4)
      val status = dataFields(5)
      val lastDate = dataFields(6)
      val grade = dataFields(7)

      val employeeData = new Employee(id, firstName, middleName, surName, joiningDate, status, lastDate, grade)
      (new AvroKey(employeeData),NullWritable.get)
    }catch {
      case t: Throwable => logger.error("Failed to perse a line");
      (null,NullWritable.get)
    }
  }
}