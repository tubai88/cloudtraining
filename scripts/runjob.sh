#!/bin/bash

if [ "$#" != "3" ]; then
  echo "Please enter 3 parameters"
  echo "Usage : $0 <InputPath> <DatasetName> <OutputPath:HDFS>"
  exit 1
fi

currentDir=`dirname $0`
if [ "${currentDir}" = "." ]; then
	basedir=`pwd`
else
	basedir=${currentDir}
fi

jarFile="${currentDir}/converter-1.0.0-uber.jar"

inputPath="$1"
dataset="$2"
inputPathHDFS="/user/cloudera/inputfile/${dataset}"
outputPath="$3"

if hdfs dfs -test -d ${inputPathHDFS} ; then
	echo "Deleting already existing HDFS path"
	hdfs dfs -rm -r -skipTrash ${inputPathHDFS}
fi
hdfs dfs -mkdir -p ${inputPathHDFS}
hdfs dfs -put ${inputPath} ${inputPathHDFS}

spark-submit \
 --master yarn --deploy-mode cluster \
 --num-executors 2 --executor-cores 2 \
 --driver-memory 1g --executor-memory 1g \
 --class com.training.cloud.data.DataConverterDriver ${jarFile} \
 ${inputPathHDFS} ${dataset} ${outputPath}
rc=$?

if [ "${rc}" != "0" ]; then
  echo "Process failed with exit code : ${rc}"
  exit ${rc}
else
  echo "Process Completed, Converted AVRO file is available in ${outputPath}"
fi

exit 0