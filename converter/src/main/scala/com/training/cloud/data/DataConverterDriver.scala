package com.training.cloud.data

import com.training.cloud.data.util.DataType
import com.training.cloud.data.exception.CustomeAppException
import com.training.cloud.data.model.Employee
import com.training.cloud.data.mapper.EmployeeMapper

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.hadoop.mapreduce.Job
import org.apache.avro.mapreduce.AvroJob
import org.apache.avro.mapreduce.AvroKeyOutputFormat
import org.apache.avro.file.DataFileConstants
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.fs.Path
import org.apache.hadoop.conf.Configuration

object DataConverterDriver {
  def main(args: Array[String]) = {
    if (args.length != 3) {
      throw CustomeAppException("Please pass 3 parameters, 1. Input File Base, 2. Input File Name, 3. Output Path base")
    }
    val dataInputPathBase = args(0)
    val dataInputFile = args(1)
    val outputPathBase = args(2)

    val dataParams = dataInputFile.split("_")
    val dataSetName = dataParams(0)
    val dataYear = dataParams(1)
    val dataMonth = dataParams(2)

    val sparkConf = new SparkConf().setAppName(dataSetName+"DataConverterDriver")
    val spark = new SparkContext(sparkConf)

    val outputPath = new Path(outputPathBase+ "/" + dataSetName + "/" + dataYear + "/" + dataMonth)

    val dataFile = spark.textFile(dataInputPathBase + "/" + dataInputFile)
    val convertedData = dataSetName.toUpperCase() match {
      case DataType.EMPLOYEE => dataFile.map { dataLine => EmployeeMapper.mapEmployee(dataLine) }
      case _                 => throw CustomeAppException("Invalid data type supplied, available data types : " + DataType.availableDataTypes())
    }

    val validRecords = convertedData.filter { x => x._1 != null }

    val jobConfiguration = new Configuration()
    jobConfiguration.set(AvroJob.CONF_OUTPUT_CODEC, DataFileConstants.SNAPPY_CODEC)
    val job = Job.getInstance(jobConfiguration)
    val schema = Employee.getClassSchema()
    AvroJob.setOutputKeySchema(job, schema)
    FileOutputFormat.setOutputPath(job, outputPath)
    FileOutputFormat.setCompressOutput(job, true)
    job.setOutputFormatClass(classOf[AvroKeyOutputFormat[Employee]])
    convertedData.saveAsNewAPIHadoopDataset(job.getConfiguration)
  }
}
